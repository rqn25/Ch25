
import java.util.Iterator;

public class Binary {

public static void main(String[] args) {
	BST<String> tree = new BST<>();
	tree.insert("George");
	tree.insert("Michael");
	tree.insert("Tom");
	tree.insert("Adam");
	tree.insert("Jones");
	tree.insert("Peter");
	tree.insert("Daniel");

	System.out.println("List of names in tree:");
	Iterator<String> iterator = tree.preorderIterator();
	while (iterator.hasNext())
	System.out.println(iterator.next());
	System.out.println("\nNumber of leaves in tree:");
	System.out.println(tree.getNumberOfLeaves());

	}

}
